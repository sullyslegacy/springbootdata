package com.sullyslegacy.controller;

import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sullyslegacy.entities.Item;
import com.sullyslegacy.repository.ItemRepository;



@RestController
public class ItemController {

	private ItemRepository itemRepository;
	
	public ItemController(ItemRepository itemRepository) {
		this.itemRepository = itemRepository; 
	}
	
	@GetMapping("/items")
	public Iterable<Item> all() {
		return itemRepository.findAll();
	}

	@PostMapping("/items")
	public Item newItem(@RequestBody Item item) {
		return itemRepository.save(item);
	}

	@GetMapping("/items/{id}")
	public Optional<Item> one(@PathVariable Long id) {
		return itemRepository.findById(id);
	}
	
}