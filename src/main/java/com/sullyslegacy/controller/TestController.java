package com.sullyslegacy.controller;

import com.sullyslegacy.animals.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TestController {

	private String foo;
    private List<Animal> animals;

    @Autowired
    public TestController( @Value("${foo}") String foo, List<Animal> animals) {
		this.foo = foo;
		this.animals = animals;

	}

	@GetMapping("/foo")
	public String test() {
		return foo;
	}

	@GetMapping("/animals")
	public List<String> getAnimals() {
		List<String> names = new ArrayList<String>();
		names.add("Animals");
		for(Animal animal : animals){
			names.add(animal.getName());
		}
		return names;
	}

}