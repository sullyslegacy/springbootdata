package com.sullyslegacy.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sullyslegacy.entities.Customer;
import com.sullyslegacy.repository.CustomerRepository;
import com.sullyslegacy.validation.CustomerValidation;



@RestController
public class CustomerController {

	private CustomerRepository customerRepository;
	private CustomerValidation customerValidator;
	
	public CustomerController(CustomerRepository customerRepository, CustomerValidation customerValidator) {
		this.customerRepository = customerRepository; 
		this.customerValidator = customerValidator;
	}
	
	@GetMapping("/customers")
	public Iterable<Customer> all() {
		return customerRepository.findAll();
	}

	@PostMapping("/customers")
	public Customer createCustomer(
			@Valid 
			@RequestBody Customer customer, BindingResult bindingResult) {
		customerValidator.validate(customer, bindingResult);

		if (bindingResult.hasErrors()) {
			return new Customer("Bad", "Customer");
		}		
		return customerRepository.save(customer);
	}

	@GetMapping("/customers/{id}")
	public Optional<Customer> one(@PathVariable Long id) {
		return customerRepository.findById(id);
	}
	
	@GetMapping("/customers/name")
	public List<Customer> findByName(@RequestParam(name="firstName", required=false) String firstName, @RequestParam(name="lastName", required=false) String lastName) {
		return customerRepository.findByName(firstName, lastName);
	}
	
}