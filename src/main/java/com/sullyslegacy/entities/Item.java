package com.sullyslegacy.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private Long customerId;
    private String itemName;

    protected Item() {}
    
    public Item(Long customerId, String itemName) {
		super();
		this.customerId = customerId;
		this.itemName = itemName;
	}

	public Long getId() {
		return id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

}