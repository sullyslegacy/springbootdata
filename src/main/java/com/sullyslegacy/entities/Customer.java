package com.sullyslegacy.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;


@Entity
public class Customer {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	
	@NotNull
    private String firstName;
	
	@NotNull
	private String lastName;

	@OneToMany(cascade = {CascadeType.ALL, CascadeType.REMOVE}, orphanRemoval = true)
	@JoinColumn(name = "customerId")
	private List<Item> items;

    protected Customer() {}

	public Customer(String firstName, String lastName) {
        this.firstName = firstName;
		this.lastName = lastName;
    }

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public List<Item> getItems(){
		return items;
	}

}