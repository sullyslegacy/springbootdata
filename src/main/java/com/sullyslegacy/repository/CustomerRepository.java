package com.sullyslegacy.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.sullyslegacy.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	List<Customer> findByFirstName(String firstName);
	List<Customer> findByLastName(String lastName);
	List<Customer> findByFirstNameAndLastName(String firstName, String lastName);
    
	default List<Customer> findByName(String firstName, String lastName){
		if(firstName == null && lastName == null) {
			return new ArrayList<Customer>();
		}
		if(firstName != null && lastName == null) {
			return findByFirstName(firstName);	
		}
		if(firstName == null && lastName != null) {
			return findByLastName(lastName);	
		}
		return findByFirstNameAndLastName(firstName, lastName);
	}

}