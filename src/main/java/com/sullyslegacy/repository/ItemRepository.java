package com.sullyslegacy.repository;

import org.springframework.data.repository.CrudRepository;

import com.sullyslegacy.entities.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {

}