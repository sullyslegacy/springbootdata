package com.sullyslegacy.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.sullyslegacy.entities.Customer;

@Component
public class CustomerValidation implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Customer.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "firstName", "First Name is required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "lastName", "Last Name is required");
	}
	
}