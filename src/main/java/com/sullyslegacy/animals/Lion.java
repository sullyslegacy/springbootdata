package com.sullyslegacy.animals;

import org.springframework.stereotype.Component;

@Component
public class Lion implements Animal {

    @Override
    public String getName(){
        return "Lion";
    }

}