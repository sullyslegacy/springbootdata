package com.sullyslegacy.animals;

import org.springframework.stereotype.Component;

@Component
public class Bear implements Animal {

    @Override
    public String getName(){
        return "Bear";
    }

}