-- schema.sql
CREATE TABLE customer(
  id     	INT PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(50),
  last_name  VARCHAR(50)
);

CREATE TABLE item(
  id     		INT PRIMARY KEY AUTO_INCREMENT,
  customer_id 	INT,
  item_name  	VARCHAR(50)
);

